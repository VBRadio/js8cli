#!/usr/bin/bash

if [[ "$1" == *"-all"* || "$1" == "" ]]; then

	cd data; find *.dat | while read file
	do

	cp "$file" plot.dat


gnuplot -p -e 'set xdata time; set timefmt "%m-%d_%H:%M"; set format x "%H:%M\n%m-%d"; set title "SNR Trend"; set terminal png size 800,600; set output "snr.png"; set nokey; plot "plot.dat" using 1:2 with lines lw 3, "plot.dat" using 1:3 with lines lw 1 lc 7, "plot.dat" using 1:4 with lines lw 1 lc 7' 2> /dev/null
#gnuplot -p -e 'set xdata time; set timefmt "%m-%d_%H:%M"; set format x "%H:%M\n%m-%d"; set title "SNR Trend"; set terminal png size 800,600; set output "snr.png"; set nokey; plot "plot.dat" using 1:2 with lines lw 3, "plot.dat" using 1:2 pt 7 lw 3 lc 7, "plot.dat" using 1:3 with lines lw 1 lc 7, "plot.dat" using 1:4 with lines lw 1 lc 7'

	rm plot.dat
	mv snr.png "$file".png
	mv "$file".png ../snr



done

else
	cd data; cp "$1" plot.dat

#gnuplot -p -e 'set xdata time; set timefmt "%m-%d_%H:%M"; set format x "%H:%M\n%m-%d"; set title "SNR Trend"; set terminal png size 800,600; set output "snr.png"; set nokey; plot "plot.dat" using 1:2 with lines lw 3, "plot.dat" using 1:2 pt 7 lw 3 lc 7, "plot.dat" using 1:3 with lines lw 1 lc 7, "plot.dat" using 1:4 with lines lw 1 lc 7'
gnuplot -p -e 'set xdata time; set timefmt "%m-%d_%H:%M"; set format x "%H:%M\n%m-%d"; set title "SNR Trend"; set terminal png size 800,600; set output "snr.png"; set nokey; plot "plot.dat" using 1:2 with lines lw 3, "plot.dat" using 1:3 with lines lw 1 lc 7, "plot.dat" using 1:4 with lines lw 1 lc 7' 2> /dev/null

	rm plot.dat
	mv snr.png "$1".png
	mv "$1".png ../snr
fi

exit



#gnuplot -p -e "set xdata time; set timefmt '%m-%d_%H:%M'; set format x '%H:%M'; set title '$1 SNR Trend'; set terminal png size 800,600; set output '$1.png'; set nokey; plot '$1.dat' using 1:2 with lines lw 3, '$1.dat' using 1:2 pt 7 lw 3 lc 7, '$1.dat' using 1:3 with lines lw 1 lc 7, '$1.dat' using 1:4 with lines lw 1 lc 7"
