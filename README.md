# js8cli contains several scipts to use JS8Call from the CLI

* **monitor** -mon -hb
* **send** tocall

* First set your call sign in **settings**
* Set your JS8 data path in **settings**  and adjust to yours
* JS8Call api control needs to be enabled for TCP listening on port 2442, and set allow to 4 connections
* Make sure you have _no_ call sign selected in the main js8call GUI program
* Set your desired send speed
* Tested with JS8Call v2.2.0


monitor examples:
* Starting "./monitor without options will only display messages directed towards your callsign.
* Starting "./monitor -mon" will display all directed traffic , but no heartbeat data.
* Starting "./monitor -mon -hb" will display all directed traffic, including heartbeat data.
* Starting "./monitor -mon -hb -data" will display all directed traffic, including heartbeat data in addition
  it will log all stations SNR for later plotting with Gnuplot (or use a specific callsign instead of '-data').
* Starting "./monitor -mon x -data" same as above but without displaying heartbeats
* Starting "./monitor x x -data" same as above but without displaying all directed traffic and heartbeats
* Traffic for @allcall will be displayed always 

send example:
* "./send N0CALL" will start up the send terminal and will send messages to the example callsign N0CALL
* "./send" will start up the send terminal with no callsign destination, and you need to set the destination
  with '/t N0CALL'
* from within the send terminal you can do "?" to list commands available

SNR data, when enabled is stored in sub directory 'data' and SNR Gnuplot images in sub directory 'snr'

# Notice

* **There are some build in functions that are specific to my station. These are automatically de-actived** 
* **..and should cause no problems with normal operations**
* Netcat *needs* to be installed and for plotting you need Gnuplot
* 'monitor' and 'send' *needs* be started from within the parent directory. These are Bash scripts and
* I recommend using a terminal multiplexer like Tmux , so you can stack 'monitor' and 'send' in one window
* Also you can do 'tail -f .band' to watch the band activity above 1050 Hz offset or 'tail -f .all' for the whole passband
 

